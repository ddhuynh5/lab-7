/*
********************************************
 * Dennis Huynh
 * CPSC 1021, Lab Section: 004, F20
 * ddhuynh@clemson.edu
 * Elliot McMillan / Matt Franchi
********************************************
 */ 

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
    srand(unsigned (time(0)));

    /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	// create array of 10
	employee empArr[10];

	// for loop to ask user for employee info
	for (int i = 0; i < 10; i++) {
	std::cout << "Enter last name of employee " << i+1 << " :" << endl;
	std::cin >> empArr[i].lastName;

	std::cout << "Enter first name of employee " << i+1 << " :" <<  endl;
	std::cin >> empArr[i].firstName;

	std::cout << "Enter birth year of employee " << i+1 << " :" <<  endl;
	std::cin >> empArr[i].birthYear;

	std::cout << "Enter hourly wage of employee " << i+1 << " :" <<  endl;
	std::cin >> empArr[i].hourlyWage;
	std::cout << "\n";

	}
	std::cout << "\n";

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

  // implement shuffle function
	 random_shuffle(&empArr[0], &empArr[10], myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
   // smaller array of 5
		employee smallEmpArr[5];
   // for loop to put shuffled info into smaller array
		for (int i = 0; i < 5; i++) {
			smallEmpArr[i] = empArr[i];
		}

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
	// implement sort function
		 sort(&smallEmpArr[0], &smallEmpArr[5], name_order);

    /*Now print the array below */
		 std::cout << "Shuffled List of Employee Info:" << endl;
		 std::cout << "\n";

		for (employee x : smallEmpArr) { //prints array with right alignment
			std::cout << setw(31) << right << x.lastName +", " + x.firstName << endl;
			std::cout << setw(31) << right << x.birthYear << endl;
			std::cout << setw(31) << right << fixed << setprecision(2) << x.hourlyWage << endl;
			std::cout << '\n';
		}
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT

	if (lhs.lastName < rhs.lastName) {
		return true;
	}
	else {
		return false;
	}
}
